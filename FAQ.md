Lemmy est-il orienté politiquement?

TL:PL: Non.

Il faut d'abord distinguer deux choses: le logiciel Lemmy, et les serveurs (aussi appelés instances) qui utilisent ce logiciel, comme par exemple le serveur Lemmy.world où la communauté (équivalent d'un sous-reddit) /c/France est hébergée.

Les deux développeurs initiaux du logiciel Lemmy ont des opinions politiques très à gauche. Ils administrent l'instance lemmy.ml.

Cependant, puisque Lemmy est un logiciel décentralisé, les administrateurs de chaque instance ont le droit de gérer leur instance comme ils l'entendent, les développeurs n'ont absolument aucun droit. Les administrateurs de Lemmy.world gèrent également mastodon.world, un des plus grand serveurs de ce service.

De plus, comme Lemmy est un logiciel libre, il est possible à tout moment de créer un copie du code pour créer une autre version, surl laquelle les développeurs initiaux n'auront aucun droit.

Références:

    Does lemmy.world allow criticism of the CCP?
    Lemmy et la censure
    Le parti pris apparent de Lemmy

Qu'est-ce que le Fediverse/Threadiverse?

Le Fédiverse est un réseau de plate-formes de réseaux sociaux.

Le Threadiverse est la sous-partie de ce réseau dédiée aux serveurs de types “agrégateurs de liens” utilisant Lemmy ou Kbin.

Concrètement, les contenus publiés sur un serveur Lemmy ou Kbin peuvent être accédés via d'autres serveurs Lemmy et Kbin. C'est un fonctionnement similaire à celui de l'email, où un utilisateur ayant une adresse Gmail peut envoyer un email à un autre ayant une adresse Outlook.

Références:

    https://framablog.org/2021/01/26/le-fediverse-et-lavenir-des-reseaux-decentralises/

Comment est financé Lemmy / Lemmy.world?

Le logiciel Lemmy est créé par des contributeurs bénévoles.

L'instance lemmy.world est financée par les dons, accessibles ici: https://opencollective.com/mastodonworld
Pourquoi venir ici plutôt que sur Reddit?

Contrairement à Reddit, Lemmy n'appartient pas à une société privée et ne cherche pas à faire du profit. Les changement annoncés en juin 2023 sur la facturation de l'API de Reddit ont montré que l'entreprise cherchait à maximiser son profit aux dépens des utilisateurs.

Références:

    https://www.lemonde.fr/pixels/article/2023/06/14/sur-reddit-la-greve-des-moderateurs-se-poursuit-au-dela-des-quarante-huit-heures-annoncees_6177609_4408996.html

Où créer un compte?

    Le plus simple est sur https://lemmy.world
    Une alternative est kbin.social, qui propose une autre interface: https://kbin.social/
    Si vous êtes curieux, vous pouvez aller faire un tour sur https://join-lemmy.org/instances pour explorer d'autres instances.

Peut-on se connecter avec un compte Pleroma/Mastodon?

Non, mais le contenu Lemmy est accessible via Pleroma et Mastodon. Par exemple:

    Un fil Lemmy: https://lemmy.world/post/258637
    Le même contenu via Pleroma : https://blah.rako.space/notice/AWqSw2IVrBosrfd7dw

Je n'aime pas l'interface de Lemmy, quelles sont mes alternatives?

    https://github.com/dbeley/awesome-lemmy/blob/master/README.md
    Utiliser Kbin: https://kbin.social/m/france@lemmy.world
    Un CSS alternatif: https://lemmy.world/post/194138
    Un autre CSS alternatif: https://lemmy.world/post/258279

Je n'arrive pas à répondre aux utilisateurs Kbin

Kbin demande de spécifier explicitement la langue pour poster. Ce n'est pour l'instant pas disponible sur mobile, il faut donc passer par l'interface web. Oui c'est ennuyeux, mais cela devrait être résolu bientôt.
Guides (en anglais)

    https://github.com/dbeley/awesome-lemmy/blob/master/README.md
    https://lemmy.world/post/37906
    https://tech.michaelaltfield.net/2023/06/11/lemmy-migration-find-subreddits-communities/

Répertoires de communautés

    https://sub.rehab
    https://redditmigration.com
    https://lemmyverse.net/communities
    https://lemmy.world/communities/listing_type/All/page/1

Applications mobiles

    Liste complète https://lemmy.world/post/465785

    https://github.com/dbeley/awesome-lemmy/blob/master/README.md

    iOS: Memmy: https://apps.apple.com/us/app/memmy-for-lemmy/id6450204299 À noter que pour iOS, les autres applications sont en beta, il faut donc installer l'application TestFlight avant de pouvoir les tester : https://apps.apple.com/us/app/testflight/id899247664?mt=8

    Android: Jerboa: https://play.google.com/store/apps/details?id=com.jerboa

Outils de navigation :

    Add-on pour faciliter la navigation entre instances : https://discuss.tchncs.de/post/76342
